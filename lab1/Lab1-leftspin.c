#pragma config(StandardModel, "EV3_REMBOT")
//*!!Code automatically generated by 'ROBOTC' configuration wizard               !!*//

/*------------------------------------------------------------------------------------------------
  Lab1-leftspin.c


This program makes the robot go forward, do a 360 degree spin and stop.

Author: Maria C., Nicolas C.
Date:30/01/2019

                            ROBOT CONFIGURATION: LEGO EV3 REM Bot
    MOTORS & SENSORS:
    [I/O Port]          [Name]              [Type]                [Description]
    MotorC        	leftMotor           LEGO EV3 Motor	  Left side motor
    MotorB       	rightMotor          LEGO EV3 Motor	  Right side motor (reversed)
------------------------------------------------------------------------------------------------*/

task main()
{

	setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to half power forward (50)
	setMotorSpeed(rightMotor, 50); 	//Set the rightMotor (motor6) to half power forward (50)
	sleep(5000);			//Continue on for 5 sec.

	//Spin!!
	setMotorSpeed(leftMotor, -50);	//Set the leftMotor (motor1) to half power forward (-50)
	setMotorSpeed(rightMotor, 50); 	//Set the rightMotor (motor6) to half power forward (50)
	sleep(1350);			//Continue on for 1.35 sec (approx for full 360 degrees turn).
	
	
}//end main
