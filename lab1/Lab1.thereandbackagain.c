#pragma config(StandardModel, "EV3_REMBOT")
/*------------------------------------------------------------------------------------------------
 Lab1-thereandbackagain.c 


This program moves the robot go forward for 5 seconds, turn 180 degrees and travel back to the same spot. 

Author: Maria C., Nicolas C.
Date: 30/01/2019 

                            ROBOT CONFIGURATION: LEGO EV3 REM Bot
    MOTORS & SENSORS:
    [I/O Port]	[Name]              [Type]                		[Location]
    MotorC		leftMotor           LEGO EV3 Motor		Left side motor
    MotorB		rightMotor         LEGO EV3 Motor		Right side motor (reversed)
------------------------------------------------------------------------------------------------*/

task main()
{
	setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to half power (50)
	setMotorSpeed(rightMotor, 50);  	//Set the rightMotor (motor6) to half power (50)
	sleep(5000);			//Wait (continue the above action) for 5 seconds. 

	
	setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to half power.
	setMotorSpeed(rightMotor, -50);  	//Set the rightMotor (motor6) to half power reverse to turn.
	sleep(700);			//Wait for 0.7 second before next step.


	setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to half power (50)
	setMotorSpeed(rightMotor, 50);  	//Set the rightMotor (motor6) to half power (50)
	sleep(5000);			//Continue for 5 seconds to come back to the same location.
}
