#pragma config(StandardModel, "EV3_REMBOT")
/*
This program decides whether the robot reverses or accelerates based on its angle.

Author: Maria C., Nicolas C.
Date:13/03/19
*/

task main()
{
	resetGyro(gyroSensor);
	while(true)
	{
			int turn=getGyroHeading(gyroSensor);
			int wheels;
			int speed;
			if(getGyroHeading(gyroSensor)<0)
			{
				speed=getGyroHeading(gyroSensor);
			}//end if left
			else
			{
				speed=getGyroHeading(gyroSensor);
			}//end if right
			displayCenteredBigTextLine(2, "%d",turn);
			setMotorSync(leftMotor, rightMotor, 0, speed);

	}//end main while

}//end main
