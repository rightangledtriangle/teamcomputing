#pragma config(StandardModel, "EV3_REMBOT")
/*This program makes the robot go in a square of preset perimeters.

Author: Maria C., Nicolas C.
Date:13/03/19
*/

task main()
{
	int dist=0;

	while(getButtonPress(buttonAny)==0)
	{
		displayCenteredBigTextLine(2, "Select length:");
		displayCenteredBigTextLine(4, "Up btn: 10cm");
		displayCenteredBigTextLine(6, "Down btn: 20cm");
		displayCenteredBigTextLine(8, "Left btn: 30cm");
		displayCenteredBigTextLine(10, "Right btn:40cm");

	}//end while for selecting button

	if(getButtonPress(buttonUp))
	{
		dist=1000;
	}//end if 10 cm
	else if(getButtonPress(buttonDown))
	{
		dist=2000;
	}//end if 20
	else if(getButtonPress(buttonLeft))
	{
		dist=3000;
	}//end if 20
	else if(getButtonPress(buttonRight))
	{
		dist=4000;
	}//end if 20

	for(int i=0;i<4;i++)
	{
		resetGyro(gyroSensor);

		setMotorSync(leftMotor, rightMotor, 0, 25);
		sleep(dist);
		while(getGyroDegrees(gyroSensor)<86)
		{
			int turn=getGyroDegrees(gyroSensor);
			displayCenteredBigTextLine(2, "%d",turn);
			setMotorSync(leftMotor, rightMotor, 100, 25);
			sleep(10);

		}//end while

	}//end for

}//end main
