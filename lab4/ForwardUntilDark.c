#pragma config(StandardModel, "EV3_REMBOT")
/*This is a program that makes the robot stop when it detects a dark line.

Author: Maria C., Nicolas C.
Date:27/02/19*/

void drive()
{
	while(getColorReflected(S3)<90){

		displayCenteredBigTextLine(4, "Black Line Detected!");
		setLEDColor(ledRedFlash);
		setMotorSync(leftMotor, rightMotor,0,0);
		sleep(5);
	}

 	displayCenteredBigTextLine(4, "Zoooom!");
	while (getColorReflected(S3)>90){

	    setMotorSync(leftMotor, rightMotor,0,20);
	     setLEDColor(ledGreenFlash);
	     sleep(5);

	 }



}

task main()
{
	while(true){

		drive();

	}


}//END MAIN
