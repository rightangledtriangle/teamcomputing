#pragma config(StandardModel, "EV3_REMBOT")
/*------------------------------------------------------------------------------------------------
 Lab1-squareroute.c 


This program makes the robot go in a large circle, ending at the starting point

Author: Maria C., Nicolas C.
Date: 30/01/2019 

                            ROBOT CONFIGURATION: LEGO EV3 REM Bot
    MOTORS & SENSORS:
    [I/O Port]	[Name]              [Type]                		[Location]
    MotorC		leftMotor           LEGO EV3 Motor		Left side motor
    MotorB		rightMotor         LEGO EV3 Motor		Right side motor (reversed)
------------------------------------------------------------------------------------------------*/

task main()
{

	for(int i=0;i<4;i++){

		//go forward
		setMotorSpeed(leftMotor, 50);	//Set the leftMotor (motor1) to half power forward (50)
		setMotorSpeed(rightMotor, 50); 	//Set the rightMotor (motor6) to half power forward (50)
		sleep(1500);			//Wait for 1.5 seconds before continuing on in the program.

		//turn at right-ish angle
		setMotorSpeed(leftMotor, 0);		//Set the leftMotor (motor1) to stop (00)
		setMotorSpeed(rightMotor, 50); 	//Set the rightMotor (motor6) to half power forward (50)
		sleep(600);			//Wait for 0.6 seconds before continuing on in the program.
	
	}//end for loop

}
