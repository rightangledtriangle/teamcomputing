#pragma config(StandardModel, "EV3_REMBOT")

/* This program counts the lines robot goes over.

Author: Maria C.,Nicolas C.
Date:27/02/2019
*/


task main()
{
	int line=0;
	int current=0;
	int last=0;
	int white=100;

	while(true){

	current=getColorReflected(S3);

	if(current==white){

		if(last<current){
			line++;
		}

			displayCenteredBigTextLine(4, "Lines: %d",line);
	    setMotorSync(leftMotor, rightMotor,0,15);
	    setLEDColor(ledGreenFlash);
	    last=current;
	    sleep(5);
	}
	else{
		if((last-current)>5){
			setMotorSync(leftMotor, rightMotor,0,0);
			sleep(500);
		}
		displayCenteredBigTextLine(4, "Black Line Detected!");
		setLEDColor(ledRedFlash);
		setMotorSync(leftMotor, rightMotor,0,5);
		last=current;
		sleep(5);

		}

	}

}//END MAIN
