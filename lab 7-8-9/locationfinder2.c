#pragma config(StandardModel, "EV3_REMBOT")
//*!This program makes the robot go to square (3,7) from (3,1).

//Author: Maria C., Nicolas C.
//Date: 3/04/2019
void goSquare(int n){

	setMotorSync(leftMotor,rightMotor,0,15);
	sleep(2000*n);

}

void turn90(int turn){
	if(turn==0){
	while(getGyroDegrees(gyroSensor)<79)
		{
			int turn=getGyroDegrees(gyroSensor);
			displayCenteredBigTextLine(2, "%d",turn);
			setMotorSync(leftMotor, rightMotor, 100, 25);
			sleep(10);
		}//end while
	}
	else{
		while(getGyroDegrees(gyroSensor)>-79)
		{
			int turn=getGyroDegrees(gyroSensor);
			displayCenteredBigTextLine(2, "%d",turn);
			setMotorSync(leftMotor, rightMotor, -100, 25);
			sleep(10);

		}//end while
	}


}
task main()
{

			goSquare(1);
			resetGyro(gyroSensor);
			turn90(0);
			goSquare(6);
			resetGyro(gyroSensor);
			turn90(0);
			goSquare(1);
			for(int i;i<2;i++) {resetGyro(gyroSensor); turn90(0);}


}
